package com.leon.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.ext.plugin.shiro.ShiroKit;

public class LoginController extends Controller {

	public void index() {
		render("/login.html");

	}
	
	@ActionKey("/logout")
	public void logout(){
		Subject currentUser = SecurityUtils.getSubject();
		if(currentUser.isAuthenticated()){
			currentUser.logout();
		}
		redirect("/");
	}

	@ActionKey("/action/login")
	public void loginAction() {

		String username = getPara("username");
		String password = getPara("password");
		Subject currentUser = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);

		try {
			currentUser.login(token);
			redirect("/");
			
		} catch (Exception e) {
			//登录失败
			forwardAction("/login");
		}
		
		

	}

}
