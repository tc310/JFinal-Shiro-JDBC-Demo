package com.leon.config;

import org.bee.tl.core.GroupTemplate;
import org.bee.tl.ext.jfinal.BeetlRenderFactory;

import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.plugin.shiro.ShiroInterceptor;
import com.jfinal.ext.plugin.shiro.ShiroPlugin;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.leon.controller.HomeController;
import com.leon.controller.LoginController;
import com.leon.model.Users;
import com.leon.tools.ShiroExt;

public class MyConfig extends JFinalConfig {

	private Routes routes;

	@Override
	public void configConstant(Constants me) {

		me.setDevMode(true);
		loadPropertyFile("classes/jfinal.properties");

		// Beetl
		me.setMainRenderFactory(new BeetlRenderFactory());
		GroupTemplate gt = BeetlRenderFactory.groupTemplate;
		gt.registerFunctionPackage("so",new ShiroExt());
		gt.setStatementStart("<%");
		gt.setStatementEnd("%>");

	}

	@Override
	public void configRoute(Routes me) {

		this.routes = me;
		me.add("/", HomeController.class);
		me.add("/login", LoginController.class);

	}

	@Override
	public void configPlugin(Plugins me) {

		String jdbcUrl = getProperty("jdbcUrl");
		String driver = getProperty("driverClass");
		String username = getProperty("username");
		String password = getProperty("password");

		// Druid
		DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, username, password, driver);
		WallFilter wallFilter = new WallFilter();
		wallFilter.setDbType("mysql");
		druidPlugin.addFilter(wallFilter);
		me.add(druidPlugin);

		// ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		arp.addMapping("users", Users.class);
		me.add(arp);

		// 加载Shiro插件
		 me.add(new ShiroPlugin(routes));

		// 缓存插件
		me.add(new EhCachePlugin());

	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new ShiroInterceptor());

	}

	@Override
	public void configHandler(Handlers me) {
		// TODO Auto-generated method stub

	}

//	public static void main(String[] args) {
//		JFinal.start("WebRoot", 8080, "/", 5);
//	}

}
